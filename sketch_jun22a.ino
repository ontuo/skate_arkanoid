#include <Keyboard.h>
//
#define S0_left 13
#define S1_left 12
#define S2_left 11
#define S3_left 10
#define SIG_1   9
#define SIG_2   8

#define S0_right 7
#define S1_right 6
#define S2_right 5
#define S3_right 4
#define SIG_3    2
#define SIG_4    3

int control_pin_left[] = {S0_left, S1_left, S2_left, S3_left};
int control_pin_right[] = {S0_right, S1_right, S2_right, S3_right};

int key_left[] = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89};
int key_right[] = {97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121};
int sensor[50];
//
int mux_channel[16][4] = {
  {0, 0, 0, 0}, //channel 0
  {1, 0, 0, 0}, //channel 1
  {0, 1, 0, 0}, //channel 2
  {1, 1, 0, 0}, //channel 3
  {0, 0, 1, 0}, //channel 4
  {1, 0, 1, 0}, //channel 5
  {0, 1, 1, 0}, //channel 6
  {1, 1, 1, 0}, //channel 7
  {0, 0, 0, 1}, //channel 8
  {1, 0, 0, 1}, //channel 9
  {0, 1, 0, 1}, //channel 10
  {1, 1, 0, 1}, //channel 11
  {0, 0, 1, 1}, //channel 12
  {1, 0, 1, 1}, //channel 13
  {0, 1, 1, 1}, //channel 14
  {1, 1, 1, 1} //channel 15
};
//xyxwvutsrqpopqrstuvwxy
void setup() {
  //Serial.begin(9600);

  pinMode(S0_left, OUTPUT);
  pinMode(S1_left, OUTPUT);
  pinMode(S2_left, OUTPUT);
  pinMode(S3_left, OUTPUT);

  digitalWrite(S0_left, LOW);
  digitalWrite(S1_left, LOW);
  digitalWrite(S2_left, LOW);
  digitalWrite(S3_left, LOW);

  pinMode(S0_right, OUTPUT);
  pinMode(S1_right, OUTPUT);
  pinMode(S2_right, OUTPUT);
  pinMode(S3_right, OUTPUT);

  digitalWrite(S0_right, LOW);
  digitalWrite(S1_right, LOW);
  digitalWrite(S2_right, LOW);
  digitalWrite(S3_right, LOW);

  pinMode(SIG_1, INPUT_PULLUP);
  pinMode(SIG_2, INPUT_PULLUP);
  pinMode(SIG_3, INPUT_PULLUP);
  pinMode(SIG_4, INPUT_PULLUP);

  //digitalWrite(SIG_1, 1);
  //digitalWrite(SIG_2, 1);
  //digitalWrite(SIG_3, 0);
  //digitalWrite(SIG_4, 0);

  Keyboard.begin();

  for (int i = 0; i < 50; i++) {
    sensor[i] = 0;
  }
}
void loop() {
  int m = 12;
  for (int i = 12; i > -1; i--) {
    for (int j = 0; j < 4; j++) digitalWrite(control_pin_left[j], mux_channel[i][j]);
    if (digitalRead(SIG_1) == 1 && sensor[m] != 1) {
      Keyboard.write(key_left[m]);
      sensor[m] = 1;
    }
    if (digitalRead(SIG_1) == 0 && sensor[m] != 0) {
      sensor[m] = 0;
    }

    m++;

  //  Serial.print( String(digitalRead(SIG_1)) + " ");
  }

 // Serial.print("| ");
  m = 0;
  for (int i = 15; i > 3; i--) {
    for (int j = 0; j < 4; j++) digitalWrite(control_pin_left[j], mux_channel[i][j]);
    if (digitalRead(SIG_2) == 1  && sensor[m] != 1) {
      Keyboard.write(key_left[m]);
      sensor[m] = 1;
    }
    if (digitalRead(SIG_2) == 0 && sensor[m] != 0) {
      sensor[m] = 0;
    }
    m++;

  //   Serial.print( String(digitalRead(SIG_2)) + " ");
  }//Serial.print("* ");
  m = 25;
  for (int i = 0; i < 13; i++) {
    for (int j = 0; j < 4; j++) digitalWrite(control_pin_right[j], mux_channel[i][j]);
    if (digitalRead(SIG_4) == 1  && sensor[m] != 1) {
      Keyboard.write(key_right[i]);
      sensor[m] = 1;
    }
    if (digitalRead(SIG_4) == 0 && sensor[m] != 0) {
      sensor[m] = 0;
    }
    m++;
//    Serial.print( String(digitalRead(SIG_4)) + " ");
  }
   //Serial.print("| ");
  m = 38;
  for (int i  = 0; i < 12; i++) {
    for (int j = 0; j < 4; j++) digitalWrite(control_pin_right[j], mux_channel[i][j]);
    if (digitalRead(SIG_3) == 1  && sensor[m] != 1) {
      Keyboard.write(key_right[i + 13]);
      sensor[m] = 1;
    }
    if (digitalRead(SIG_3) == 0 && sensor[m] != 0) {
      sensor[m] = 0;
    }
    m++;
   // Serial.print( String(digitalRead(SIG_3)) + " ");
  }

 // Serial.println("");
}
